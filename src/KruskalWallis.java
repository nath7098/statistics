import java.util.ArrayList;
import java.util.Collections;

public class KruskalWallis {

    private TableE tableE = new TableE();
    private ArrayList<Groupe> groupes = new ArrayList<>();
    private ArrayList<Integer> tri = new ArrayList<>();
    private ArrayList<Double> rank = new ArrayList<>();

    public KruskalWallis(int nbGroupes) {
        for(int i = 0; i < nbGroupes;i++){
            Groupe g = new Groupe();
            g.InsertData();
            groupes.add(g);
            for(int j = 0; j < g.getData().size(); j++){
                tri.add(g.getData().get(j));
            }
        }
        Collections.sort(tri);
        for(int i = 1, k = 0; i < tri.size(); i++){
            if(tri.get(i).equals(tri.get(i-1))) {
                rank.add(i-1,i + k +0.5);
                rank.add(i, i + k + 0.5);
            }
            else {
                rank.add(i + k + 0.0);
            }
        }

        System.out.println(tri);
        System.out.println(rank);

        int numRank = 0;
        for(Groupe g : groupes) {
            for(int i = 0; i < g.getData().size();i++, numRank++) {
                g.setRank(g.getRank()+rank.get(numRank));
            }
        }
        for(int i = 0; i < groupes.size(); i++) {
            System.out.println(groupes.get(i).getData());
            System.out.println(groupes.get(i).getRank());
        }
    }

}
