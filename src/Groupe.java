import java.util.ArrayList;
import java.util.Scanner;

public class Groupe {

    private ArrayList<Integer> data;
    private int dataSize;
    private double rank;

    public Groupe() {
        data = new ArrayList<>();
        dataSize = 0;
        rank = 0;
    }

    public Groupe(int dataSize) {
        data = new ArrayList<>();
        this.dataSize = dataSize;
        rank = 0;
    }

    public void InsertData() {
        Scanner sc = new Scanner(System.in);
        if(dataSize==0) {
            System.out.println("input the size of data :");
            dataSize = sc.nextInt();
            sc.nextLine();
        }
        System.out.println("input data :");
        for(int i = 0 ; i < dataSize; i++){
            data.add(sc.nextInt());
            sc.nextLine();
        }
    }

    public ArrayList<Integer> getData() {
        return data;
    }

    public int getDataSize() {
        return dataSize;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }
}
