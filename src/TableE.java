import java.util.HashMap;
import java.util.List;

public class TableE {

    private HashMap<Integer, Integer> table;

    public TableE() {
        table = new HashMap<>();
        table.put(3,1);
        table.put(4,1);
        table.put(5,1);
        table.put(6,1);
        table.put(7,2);
        table.put(8,2);
        table.put(9,2);
        table.put(10,3);
        table.put(11,3);
        table.put(12,4);
        table.put(13,4);
        table.put(14,4);
        table.put(15,5);
        table.put(16,5);
        table.put(17,5);
        table.put(18,6);
        table.put(19,6);
        table.put(20,7);
        table.put(21,7);
        table.put(22,8);
        table.put(23,8);
        table.put(24,8);
        table.put(25,9);
        table.put(26,9);
        table.put(27,10);
        table.put(28,10);
        table.put(29,10);
        table.put(30,11);
    }

    public HashMap<Integer, Integer> getTable() {
        return table;
    }
}
