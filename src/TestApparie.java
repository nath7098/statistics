import java.util.ArrayList;

public class TestApparie {

    private TableE tableE = new TableE();
    private Groupe grp1;
    private Groupe grp2;
    private ArrayList<Integer> difference = new ArrayList<>();
    private int Sp = 0;     //S+
    private int Sm = 0;     //S-
    private int d = 0;      //d from table E

    public TestApparie(int dataSize) {
        grp1 = new Groupe(dataSize);
        grp2 = new Groupe(dataSize);
        System.out.println("Getting data \n");
        grp1.InsertData();
        grp2.InsertData();
        System.out.println("Getting difference \n");
        calcDifference();
        System.out.println("Testing sign \n");
        testSigne();
        System.out.println("Searching d for n="+ difference.size() + " and alpha' = 0.05 \n");
        getD();
        HypothesisValidation();
    }

    private void calcDifference() {
        for(int i = 0; i < grp1.getDataSize(); i++){
            difference.add(grp1.getData().get(i)-grp2.getData().get(i));
        }
        System.out.println(difference);
    }

    private void testSigne() {
        for(int i = 0; i < difference.size(); i++) {
            if(difference.get(i)<0)
                Sm++;
            else
                Sp++;
        }
        System.out.println("S+ = "+ Sp + " S- = " + Sm);
    }

    private void getD() {
        d = tableE.getTable().get(difference.size());
        System.out.println("d = " + d + "\n");
    }

    private void HypothesisValidation() {
        if(Sp < d || Sm < d){
            System.out.println("reject H0");
        }
        else{
            System.out.println("Accept H0");
        }
    }

}
